# Vagrant and Openstack

## Clone this repo
```
git clone git@gitlab.ics.muni.cz:172673/vagrant-openstack-example.git
cd vagrant-openstack
```

## Download and install Vagrant
https://www.vagrantup.com/downloads.html

## Install openstack-provider plugin
`vagrant plugin install vagrant-openstack-provider`

## Download V3 RC file for your Openstack project
https://ostack.ics.muni.cz/horizon/project/access_and_security/api_access/openrc/

Rename the downloaded file to `openrc-v3.sh`

## Set up your bash environment and run the provisioning

* Modify [Vagrantfile](Vagrantfile) accordingly
  * Consult Vagrant docs
  * Consult https://github.com/ggiamarchi/vagrant-openstack-provider
* Export environment via Openstack RC file
  * `source openrc-v3.sh`
* Provision your machines via vagrant
  * `vagrant up`

## Profit
* Please help to improve this page by adding anything of value
* Use the mechanism of fork-and-merge-request OR create a new issue